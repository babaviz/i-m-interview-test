package com.im.test.question2;

// Class Binary Search 
public class BinarySearch {

    public int search(int haystack[],  int searchKey){
        //keep original copy before formatting
        int []original=haystack.clone();

        //sort the collection in ascending order
        bubbleSort(haystack);

        int beginning=0;//start point of the search
        int end=haystack.length-1;//last /max index

        int mid = (beginning + end)/2;
        while( beginning <= end ){
            if ( haystack[mid] < searchKey ){
                beginning = mid + 1;
            }else if ( haystack[mid] == searchKey ){
                int index=findOriginalIndex(original,searchKey);
                System.out.println("Element is found at index: " + index);
                return index;
            }else{
                end = mid - 1;
            }
            mid = (beginning + end)/2;
        }
        //if out of loop, then we did get what we wanted
        System.out.println("Element is not found!");
        return -1;
    }

    //sort collection in ascending order
    private int [] bubbleSort(int[] vals) {
        int length = vals.length;
        int temp;
        for(int i=0; i < length; i++){
            for(int j=1; j < (length-i); j++){
                if(vals[j-1] > vals[j]){
                    //swap elements
                    temp = vals[j-1];
                    vals[j-1] = vals[j];
                    vals[j] = temp;
                }

            }
        }
        return vals;
    }

    private int findOriginalIndex(int [] original,int val){
        for(int i=0;i<original.length;i++){
            if(original[i]==val){
                return i;//original index found
            }
        }
        return -1;//not found
    }

}
