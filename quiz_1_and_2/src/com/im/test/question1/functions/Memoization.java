package com.im.test.question1.functions;

import com.im.test.question1.interfaces.PrimenumberInterface;

import java.util.HashMap;
import java.util.Map;

/**
 * In computing, memoization or memoisation is an optimization technique used primarily
 * to speed up computer programs by storing the results of expensive function calls and
 * returning the cached result when the same inputs occur again
 */
public class Memoization {
    //use Map as a store for each method function
    private Map<Integer,String> cache=new HashMap<>();


    //method to calculate and memorize primefunction
    public void memoize(PrimenumberInterface function,int number){
        if(!cache.containsKey(number)){//if we don't have this version of our function, calculate and store
            function.isPrimeNumber(number);
            cache.put(number,((Prime)function).getMsg());
        }else{
            System.out.println("Memoized already----");
        }
        System.out.println(cache.get(number));//return what we have memorized
    }
}
