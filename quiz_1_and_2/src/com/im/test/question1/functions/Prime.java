package com.im.test.question1.functions;

import com.im.test.question1.interfaces.PrimenumberInterface;

public class Prime implements PrimenumberInterface {
    private String msg;
    @Override
    public boolean isPrimeNumber(int num) {
        if(num==0||num==1 || num==-1){// zero and one are not prime numbers
            msg=num+" is not prime number";
            return false;
        }else{
            for(int i=2;i<=num/2;i++){//loop till half way of the number, half is the largest divisor that can produce a whole number
                if(num%i==0){//if divisible, then it's not
                    msg=num+" is not prime number";
                    return false;
                }
            }
            //return true if number is not divisible
            msg=num+" is prime number";
            return true;
        }
    }

    public String getMsg() {
        return msg;
    }
}
