package com.im.test.question1;

import com.im.test.question1.functions.Memoization;
import com.im.test.question1.functions.Prime;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Enter none int to quit");
        Scanner sc=new Scanner(System.in);
        Memoization memoization=new Memoization();
        int input=0;
        do {// loop as long as the user issues a correct input
            try{//protected code, if an error occurs, exit
                System.out.print("Enter number:");
                input=sc.nextInt();
                memoization.memoize(new Prime(),input);
            }
            catch (InputMismatchException ex){
                break;// not a number, so exit
            }
            System.out.println("--------------------");//output separator

        }while (true);
    }
}
