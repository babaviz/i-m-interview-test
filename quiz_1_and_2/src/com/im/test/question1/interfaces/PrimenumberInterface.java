package com.im.test.question1.interfaces;

public interface PrimenumberInterface {
    boolean isPrimeNumber(int num);
}
