
$(function () {
    let d_url=getParameterByName("detail");
    if(d_url){
        fetchAndTabulate(d_url,"details", function (data) {
            let name="";
            if(data.name){
                name=data.name;
            }
            $("#bookmark").attr("data-url",d_url);
            $("#bookmark").attr("data-name",name);
            $("#bookmark").on("click",function (e) {
               let key=$(this).attr("data-url");
               let val=$(this).attr("data-name");
               console.log(key+":"+val);
               addToFavorite(key,val);
            });
        });
    }else{
        alert("unknown request");
    }

});


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function fetchAndTabulate(url,id,func) {
    $.get(decodeURI(url),function (data, status) {
       if(status=="success"){
            tabulate(id,data);
            if(func){
                func(data);
            }
       } else {
           alert("Oops! something went wrong!");
       }
    });
}

function tabulate(id,data) {
    let res=data;
    if(data.results){
        res=data.results;
    }
    var table="<table class='table table-bordered table-hover table-condensed'><tbody>";
    Object.entries(res).forEach(
        ([data_key, data_val]) => {
            if(Array.isArray(data_val)){
                let list="<ul>";
                for (let obj of data_val){
                    list+="<li>"+isLink(obj)+"</li>";
                }
                list+="</ul>";

                table+=`<tr>
                            <th>`+format(data_key.toUpperCase())+`</th>
                            <td>`+list+`</td>
                        </tr>`;
            }else{
                table+=`<tr>
                        <th>`+format(data_key.toUpperCase())+`</th>
                        <td>`+isLink(data_val)+`</td>
                    </tr>`;
            }
        }
    );
    table+="</tbody></table>";
    $("#"+id).html(table);

    $(".more").on("click",function (e) {
        $('.modal').modal('hide');// first close all

        let text=$(this).text();
        fetchAndTabulate(text,"more",function (data) {
            console.log(data);
            if(data.name){
                $("#title").text(data.name.toUpperCase());
            }else if(data.title){
                $("#title").text(data.title.toUpperCase());
            }
            $("#myModal").modal("show");
        });
    })
}

function format(string) {
    return string.replace(/_/g, ' ');
}

const isLink = (string) => {
    try {
        new URL(string);
        return "<a class='more'>"+string+"</a>";
    } catch (_) {
        return string;
    }
};

$( document ).ajaxComplete(function() {
    console.log( "Triggered ajaxComplete handler." );
    $("#loading").css("display","none");
});

$( document ).ajaxStart(function() {
    console.log( "Triggered start handler." );
    $("#loading").css("display","block");
});


