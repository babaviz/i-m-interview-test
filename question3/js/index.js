const showCharacterAttr=function (data) {
    let count = data.count;
    let nextUrl=data.next;
    let previousUrl=data.previous;
    let page=1;
    if(nextUrl !=null){
        page=Number(getParameterByName('page',nextUrl))-1;
    }else{
        page=Number(getParameterByName('page',previousUrl))+1;
    }
    console.log("page:"+page);
    let num=((page-1)*10)+1;

    Object.entries(data.results).forEach(
        ([entry_key, entryVal]) => {
            $("#root_data").append(
                `<tr data-href="`+encodeURI(entryVal['url'])+`">
                    <td>` + (num++) + ` &nbsp;&raquo; `+isFavorited(encodeURI(entryVal['url']))+`</td>
                    <td>` + entryVal['name'] + `</td>
                    <td>` + entryVal['gender'] + `</td>
                    <td>` + entryVal['skin_color'] + `</td>
                    <td>` + entryVal['birth_year'] + `</td>
                    <td><a href="details.html?detail=`+encodeURI(entryVal['url'])+`" target="_blank">details</a></td>
                </tr>`
            );

        }
    );

    pagger(count,page,nextUrl,previousUrl);

    $("tr").on("click",function (e) {
        let link=$(this).attr("data-href");
        console.log("tr clicked:"+link);
        if(link){
            window.open("details.html?detail="+link, '_blank');
        }
    });
};


function pagger(total,page, next, previous) {
    $("#status").text("page "+page+ " of "+Math.ceil(total/10));
    if(next!=null){
        $("#next").css("display","inline");
        $("#next a").attr("data-href", next)
    }else{
        $("#next").css("display","none");
    }
    if(previous!=null){
        $("#prev").css("display","inline");
        $("#prev a").attr("data-href", previous)
    }else{
        $("#prev").css("display","none");
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


$(function () {
    fetchData("https://swapi.co/api/people",showCharacterAttr);


});



function fetchData(url, func) {
    console.log(url);
    $.get(url, function (data, status) {
        console.log(data);
        if (status == 'success') {
            func(data);
        } else {//failed
            console.log(status);
            alert("Oops! something went wrong!");
        }
    });
}

function loadpage(a) {
    let href=a.attr("data-href");
    console.log(href);
    fetchData(href,function (data) {
        if(data){
            $("#root_data").html("");
            showCharacterAttr(data);
        }
    })
}

$( document ).ajaxComplete(function() {
    console.log( "Triggered ajaxComplete handler." );
    $("#loading").css("display","none");
});

$( document ).ajaxStart(function() {
    console.log( "Triggered start handler." );
    $("#loading").css("display","block");
});