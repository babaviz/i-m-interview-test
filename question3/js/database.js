
let itemsArray = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

localStorage.setItem('items', JSON.stringify(itemsArray));
const liMaker = (object) => {
    $("#favorites").append(
        `<li class="list-group-item" data-url="`+object.key+`"><i class="fa fa-heart"></i>&nbsp; `+object.val+`<span class="rmv">X</span></li>`
    );
};

function addToFavorite (key,val) {
    //check limit
    if(itemsArray.length==5){
        alert("Reached limit!");
        return;
    }
    //check duplicates
    for (let obj of itemsArray) {
        if(obj.key==key){
            return;
        }
    }
    //insist on validation
    if(itemsArray.length<5) {
        itemsArray.push({key: key, val: val});
        localStorage.setItem('items', JSON.stringify(itemsArray));
        liMaker({key: key, val: val});
        setBookmarked();
    }
}

var keyrm = getParameterByName("detail");

function setup() {
    $("#favorites").html("");
    itemsArray.forEach(item => {
        liMaker(item);
        if (item.key == keyrm) {
            setBookmarked();
        }
    });
}

function setBookmarked(){
    $("#bookmark").css("background","orange");
    $("#bookmark").html(
        `<i class="fa fa-heart"></i>`
    );
    $("#unbook").css("display","inline");
}

function remove(key) {
    for( var i = 0; i < itemsArray.length; i++){
        if ( itemsArray[i].key == key) {
            itemsArray.splice(i, 1);
        }
    }
    setup();
    localStorage.setItem('items', JSON.stringify(itemsArray));
    window.location.reload(true);
}

$(function () {
    setup();
   $(".rmv").on("click",function (e) {
       e.preventDefault();
       if (typeof e.stopPropagation != "undefined") {
           e.stopPropagation();
       } else {
           e.cancelBubble = true;
       }
       let key=$(this).closest("li").attr("data-url");
       console.log("key:"+key);
       remove(key);
   });

   $("li.list-group-item").on("click",function (e) {
       console.log("li clicked");
       $(location).attr('href', "details.html?detail="+$(this).attr("data-url"));
   })
});

function unfavorite() {
    remove(keyrm);
    $("#unbook").css("display","none");
}

function isFavorited(key) {
    for (let obj of itemsArray) {
        if(obj.key==key){
            return "<i class='fa fa-heart'></i>";
        }
    }

    return"";
}

